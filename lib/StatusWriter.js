'use strict';

let _pjson = null;
let utils = require('simpleutils');

function __formatStatus(v) {
  if (v === null) { return utils.format('iniciando...', 'yellow'); }
  return (v) ?
    utils.format('ok', 'green') :
    utils.format('erro', 'red');
}

let _ = {
  'borderColor': 'green+bold',
  'border': function (str, color) {
    return utils.format(str, color || _.borderColor);
  },
  'removeFormat': function (str) {
    let tRE = new RegExp('\\033\\[[0-9]+m', 'g'); return str.replace(tRE, '');
  },

  'repeat': function (s, n) { return new Array(parseInt(n+1, 10)).join(s); },
  'debugBox': function (msgs, alignments, printfn, borderColor) {
    let cleanMsgs = msgs.map((a) => utils.removeFormat(a));
    let baseLen = cleanMsgs.reduce((a, b) => Math.max(a, b.length), 0);
    _.debugBoxBoundary(baseLen, printfn, borderColor);
    msgs.forEach((msg, i) => _.debugBoxLine(
      msg, cleanMsgs[i], baseLen, alignments[i], printfn, borderColor
    ));
    _.debugBoxBoundary(baseLen, printfn, borderColor);
  },
  'debugBoxBoundary': function (contentSize, printfn, borderColor) {
    let line = '+'+ _.repeat('-', contentSize) +'+';
    printfn(_.border(line, borderColor));
  },
  'debugBoxLine': function (msg, cleanMsg, lineSize, alignment, printfn, borderColor) {
    let paddingLeft = '', paddingRight = '';
    if (lineSize) {
      let n = lineSize -cleanMsg.length;
      if (n > 0) {
        //center
        if (alignment === 1) {
          paddingLeft = _.repeat(' ', Math.floor(n /2));
          paddingRight = _.repeat(' ', Math.ceil(n /2));
        //left
        } else if (alignment === 0 || typeof alignment === 'undefined') {
          paddingRight = _.repeat(' ', n);
        //right
        } else if (alignment === 2) {
          paddingLeft = _.repeat(' ', n);
        } else {
          throw new Error('Invalid alignment value.');
        }
      }
    }
    let line = paddingLeft + msg + paddingRight;
    return printfn(_.border('|', borderColor) + line + _.border('|', borderColor));
  }
};
let statusWriter = {
  'configure': function (pjson) { _pjson = pjson; },

  'showStatus': function (curStatus, printfn, borderColor) {
    let alignments = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
    let preMsgs = [
      ' '+ _pjson.name +' ',
      ' version '+ _pjson.version +' '];
    let postMsgs = ['', ' Status: '+ __formatStatus(curStatus) +' '];

    let midMsgs = (curStatus === null) ?
      ['',
      '         o',
      'foca no / |',
      ' código | \\',
      '    .   |  |',
      '  .\'\\`  | \\|',
      '   | \\_/ \\ \\',
      '    \\____/\\/'] :
      [];
    let msgs = preMsgs.concat(midMsgs, postMsgs);
    _.debugBox(msgs, alignments, printfn, borderColor);
  }
};

module.exports = statusWriter;
